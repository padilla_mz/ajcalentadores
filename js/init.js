 $(document).ready(function(){
    $('.parallax').parallax();
    $('.button-collapse').sideNav({
    	menuWidth: 300,
    });
    $('.dropdown-btn').dropdown({
    	constrain_width: false,
    	belowOrigin: true,
    	hover: false,
    });
    $('.scrollspy').scrollSpy();
    // $('.nav-point').pushpin({top: $('.nav-point').offset().top });

    var waypoint = new Waypoint({
        element: $('.point1'),
        handler: function(direction){
            if (direction == 'down') {
                $('.link-top').addClass('animated bounceInUp');
            }
            else {
                $('.link-top').removeClass('animated bounceInUp');
            };
        }
    });
});